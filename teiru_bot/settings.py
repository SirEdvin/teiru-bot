import dynaconf
import structlog
import structlog.processors

settings = dynaconf.Dynaconf(
    validators=[
        dynaconf.Validator("DISCORD_BOT_TOKEN", must_exist=True, is_type_of=str),
        dynaconf.Validator("DISCORD_BOT_PREFIX", is_type_of=str, default="$"),
        dynaconf.Validator("DISCORD_LEADER_ROLE_LIST", is_type_of=list, default=["РЛ-Альянс", "РЛ-Орда"]),
        dynaconf.Validator("DISCORD_SQUAD_ROLE_PREFIX", is_type_of=str, default="РБГ"),
        dynaconf.Validator("DISCORD_CHANNEL_RESTRICTION", is_type_of=str, default=""),
        dynaconf.Validator("ADMIN_DISCORD_CHANNEL_RESTRICTION", default="bot-power"),
        dynaconf.Validator("DISCORD_MEMBER_NAME_PATTERN", is_type_of=str, default=r'\w+ \(\w\w\).*'),
        dynaconf.Validator("DISCORD_MEMBER_REPLACE_NAME", is_type_of=str, default="Напиши игровой ник (сервер)"),
        dynaconf.Validator("DISCORD_AUTHORIZED_ROLES", default='["Присоединение", "Резидент", "Модератор", "Класслидер", "РЛ", "Администратор"]')
    ],
    envvar_prefix="TEIRU",
    settings_files=[],
)

structlog.configure(
    processors=[structlog.processors.TimeStamper(fmt="iso"), structlog.processors.KeyValueRenderer()],
)
