import operator
import typing
import re

import discord
from discord.ext import commands

from .settings import settings

intents = discord.Intents.default()
intents.members = True  # pylint: disable=assigning-non-slot
bot = commands.Bot(command_prefix=settings.DISCORD_BOT_PREFIX, intents=intents)


# TODO: rename and reorganize
class Helper:

    @classmethod
    async def access_check(cls, ctx: commands.Context, allow_admin: bool = False) -> bool:
        if settings.DISCORD_CHANNEL_RESTRICTION and ctx.channel.name != settings.DISCORD_CHANNEL_RESTRICTION:
            return False
        if not Helper.is_leader(ctx.author):
            if ((ctx.author.guild_permissions.administrator or ctx.author.guild_permissions.manage_roles) and not allow_admin):
                await ctx.send("Так-с, я слишком тупой для таких запросов, права у тебя есть, вот и разруливай сам")
            else:
                await ctx.send("Молодой человек, это явно не для вас написано!")
            return False
        return True

    @classmethod
    def _build_leader_role_filter(cls) -> typing.Callable[[discord.Role], bool]:
        def _is_role_is_leader_role(role: discord.Role) -> bool:
            return role.name in settings.DISCORD_LEADER_ROLE_LIST

        return _is_role_is_leader_role

    @classmethod
    def extract_leader_role(cls, member: discord.Member) -> typing.Optional[discord.Role]:  # pylint: disable=unsubscriptable-object
        role = next(filter(cls._build_leader_role_filter(), member.roles), None)
        return role

    @classmethod
    def is_leader(cls, member: discord.Member) -> bool:
        return any(filter(cls._build_leader_role_filter(), member.roles))

    @classmethod
    def get_available_squads(cls, member: discord.Member) -> typing.List[discord.Role]:
        if not cls.is_leader(member):
            return []
        return list(
            filter(lambda role: role.name.startswith(settings.DISCORD_SQUAD_ROLE_PREFIX), member.roles)
        )

    @classmethod
    def check_member_name(cls, member: discord.Member) -> bool:
        return re.match(settings.DISCORD_MEMBER_NAME_PATTERN, member.name) is not None

    @classmethod
    async def rename_member_on_join(cls, member: discord.Member) -> None:
        if not cls.check_member_name(member):
            await member.edit(nick=settings.DISCORD_MEMBER_REPLACE_NAME)


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))


@bot.event
async def on_member_join(member: discord.Member):
    await Helper.rename_member_on_join(member)


@bot.command()
async def myinfo(ctx: commands.Context):
    if await Helper.access_check(ctx):
        header = "Приветствую тебя, {0} {1.mention}\nВот отряды в твоем подчинении:\n".format(
            Helper.extract_leader_role(ctx.author), ctx.author
        )
        footer = "\n".join(map(operator.attrgetter('mention'), Helper.get_available_squads(ctx.author)))
        await ctx.send(header + footer)


@bot.command()
async def hire(ctx, member: discord.Member, squad: discord.Role):
    if await Helper.access_check(ctx):
        available_roles = Helper.get_available_squads(ctx.author)
        if squad not in available_roles:
            await ctx.send("Я, конечно, все понимаю, но вот зачем пытатся управлять чужими отрядами, а?")
            return
        if squad in member.roles:
            await ctx.send("Бедняга уже в твоем отряде, остановись!")
            return
        await member.add_roles(squad)
        await ctx.send("Готово, {0.mention} теперь в {1}".format(member, squad))


@bot.command()
async def dismis(ctx: commands.Context, member: discord.Member, squad: discord.Role):
    if await Helper.access_check(ctx):
        available_roles = Helper.get_available_squads(ctx.author)
        if squad not in available_roles:
            await ctx.send("Я, конечно, все понимаю, но вот зачем пытатся управлять чужими отрядами, а?")
            return
        if squad not in member.roles:
            await ctx.send("Бедняга уже покинул твой отряд, остановись!")
            return
        await member.remove_roles(squad)
        await ctx.send("Готово, {0.mention} больше не состоит в {1}".format(member, squad))


@bot.command(name="list")
async def member_list(ctx: commands.Context, role: discord.Role):
    if await Helper.access_check(ctx, allow_admin=True):
        members = ",".join(
            member.mention for member in ctx.guild.members
            if any(m_role.name == role.name for m_role in member.roles)
        )
        await ctx.send(f"Список учасников: {members}")


def start_bot():
    bot.run(settings.DISCORD_BOT_TOKEN)
