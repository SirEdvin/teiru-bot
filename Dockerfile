FROM python:3.9.1

COPY ["./Pipfile", "./Pipfile.lock", "/app/"]
WORKDIR /app

RUN pip install pipenv && pipenv install --system --deploy

COPY "teiru_bot/" "/app/teiru_bot"
COPY "start.py" "/app"

CMD ["python3", "start.py"]
