lint:
	pylint teiru_bot
	pycodestyle teiru_bot
	mypy teiru_bot --ignore-missing-imports
inspect:
	pipenv check
	bandit -r teiru_bot
